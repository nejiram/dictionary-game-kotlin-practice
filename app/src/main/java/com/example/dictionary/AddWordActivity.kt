package com.example.dictionary

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_word.*
import java.io.PrintStream

class AddWordActivity : AppCompatActivity() {

    private val EXTRA_WORDS_FILE = "extra_words.txt"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_word)
    }

    fun addTheWord(view: View) {
        val word = word_to_add.text.toString()
        val definition = word_definition.text.toString()
        val line = "$word\t$definition"

        val stream = PrintStream(openFileOutput(EXTRA_WORDS_FILE, Context.MODE_APPEND))
        stream.use {
            stream.println(line)
        }

        Toast.makeText(this, "New word added!", Toast.LENGTH_SHORT).show()

        val intent = Intent()
        intent.putExtra("word", word)
        intent.putExtra("definition", definition)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}