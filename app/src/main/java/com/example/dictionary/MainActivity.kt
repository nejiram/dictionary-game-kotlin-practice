package com.example.dictionary

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.view.get
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.math.log

class MainActivity : AppCompatActivity() {

    private val ADD_WORD = 1

    private val defns = mutableListOf<String>()
    private val wordToDefn = HashMap<String, String>()
    private val words = mutableListOf<String>()
    private lateinit var correctAns: String
    private lateinit var myAdapter: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        readDictionaryFile(Scanner(resources.openRawResource(R.raw.dictionary)))
        readDictionaryFile(Scanner(openFileInput("extra_words.txt")))

        listSetup()
        myAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, defns)
        definitions_list.adapter = myAdapter

        definitions_list.setOnItemClickListener { _, _, index, _ ->
            val ans = definitions_list.getItemAtPosition(index)
            if (ans == correctAns) {
                Toast.makeText(this, "Correct Answer!", Toast.LENGTH_SHORT).show()
                listSetup()
                myAdapter.notifyDataSetChanged()
            } else {
                Toast.makeText(this, "Wrong!", Toast.LENGTH_SHORT).show()
                listSetup()
                myAdapter.notifyDataSetChanged()
            }
        }

    }

    private fun readDictionaryFile(reader: Scanner) {
        while (reader.hasNextLine()) {
            val line = reader.nextLine()
            val pieces = line.split("\t")
            words.add(pieces[0])
            wordToDefn[pieces[0]] = pieces[1]
        }
    }

    private fun listSetup() {

        val r = Random()
        val index = r.nextInt(words.size)
        val word = words[index]
        correctAns = wordToDefn[word]!!
        the_word.text = word

        defns.clear()
        defns.add(wordToDefn[word]!!)
        words.shuffle()
        for (otherWord in words.subList(0, 5)) {
            if (otherWord == word || defns.size == 5)
                continue
            defns.add(wordToDefn[otherWord]!!)
        }

        defns.shuffle()
    }

    fun addWordButtonClick(view: View) {
        val intent = Intent(this, AddWordActivity::class.java)
        startActivityForResult(intent, ADD_WORD)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ADD_WORD) {
            if (data != null) {
                val word = data.getStringExtra("word")
                val definition = data.getStringExtra("definition")
                wordToDefn[word] = definition
                words.add(word)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("ans", correctAns)
        outState.putString("word", the_word.text.toString())
        val defnsArray = ArrayList<String>()
        for (d in defns) {
            defnsArray.add(d)
        }
        outState.putStringArrayList("definitions", defnsArray)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        correctAns = savedInstanceState.getString("ans").toString()
        the_word.text = savedInstanceState.getString("word")
        val defnsArray = savedInstanceState.getStringArray("definitions")
        defns.clear()
        if (defnsArray != null) {
            for (d in defnsArray) {
                defns.add(d)
            }
            myAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, defns)
            definitions_list.adapter = myAdapter
        }
    }

}